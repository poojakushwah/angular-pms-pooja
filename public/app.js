var app = angular.module('myApp', ['ui.router', 'ui.router.compat', 'frontendApp', 'administratorApp']);

app.run(function($rootScope) {
    //$rootScope.bodyClass = '';
});

app.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
});
