<!DOCTYPE html>
<html lang="en" ng-app="myApp">

<head>
    <title></title>
    <script src="<?php echo asset('assets/libs/jquery.min.js')?>"></script>
    <script src="<?php echo asset('assets/libs/angular.js')?>"></script>
    <script src="<?php echo asset('assets/libs/angular-ui-router.min.js') ?>"></script>

    <script type="text/javascript" src="<?php echo asset('app/component/site/site.module.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset('app/component/admin/products/product.module.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset('app/component/admin/productCategories/productcategory.module.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset('app/component/admin/users/user.module.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset('app/component/admin/admin.module.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset('app.js') ?>"></script>
</head>

<body ui-view>
</body>

</html>
